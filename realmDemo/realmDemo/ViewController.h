//
//  ViewController.h
//  realmDemo
//
//  Created by Aaron Peng on 16/8/22.
//  Copyright © 2016年 BUPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *ageTextField;
@property (weak, nonatomic) IBOutlet UISwitch *genderSwitch;

@property (weak, nonatomic) IBOutlet UITextField *dogsNameTextField;


@end

