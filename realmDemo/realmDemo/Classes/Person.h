//
//  Person.h
//  realmDemo
//
//  Created by Aaron Peng on 16/8/22.
//  Copyright © 2016年 BUPT. All rights reserved.
//

#import <Realm/Realm.h>
#import "Dog.h"

@interface Person : RLMObject

@property NSInteger id;
@property NSString *name;
@property int age;
@property BOOL isMale;
@property RLMArray <Dog*> <Dog> *dogs;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<Person>
RLM_ARRAY_TYPE(Person)
