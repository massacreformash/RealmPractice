//
//  Person.m
//  realmDemo
//
//  Created by Aaron Peng on 16/8/22.
//  Copyright © 2016年 BUPT. All rights reserved.
//

#import "Person.h"

@implementation Person

+ (NSString *)primaryKey {
    return @"id";
}

// Specify default values for properties

//+ (NSDictionary *)defaultPropertyValues
//{
//    return @{};
//}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

@end
