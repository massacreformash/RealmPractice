//
//  Dog.m
//  realmDemo
//
//  Created by Aaron Peng on 16/8/26.
//  Copyright © 2016年 BUPT. All rights reserved.
//

#import "Dog.h"
#import "Person.h"

@implementation Dog

+ (NSString *)primaryKey {
    return @"id";
}

+ (NSDictionary *)linkingObjectsProperties {
    return @{
             @"owners": [RLMPropertyDescriptor descriptorWithClass:Person.class propertyName:@"dogs"],
             };
}

// Specify default values for properties

//+ (NSDictionary *)defaultPropertyValues
//{
//    return @{};
//}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

@end
