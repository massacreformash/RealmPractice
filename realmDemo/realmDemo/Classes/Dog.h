//
//  Dog.h
//  realmDemo
//
//  Created by Aaron Peng on 16/8/26.
//  Copyright © 2016年 BUPT. All rights reserved.
//

#import <Realm/Realm.h>

@interface Dog : RLMObject

@property NSInteger id;
@property NSString *name;
@property (readonly) RLMLinkingObjects *owners;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<Dog>
RLM_ARRAY_TYPE(Dog)
