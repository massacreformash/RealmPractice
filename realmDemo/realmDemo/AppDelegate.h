//
//  AppDelegate.h
//  realmDemo
//
//  Created by Aaron Peng on 16/8/22.
//  Copyright © 2016年 BUPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

